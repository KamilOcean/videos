import Vue from 'vue'
import App from './App.vue'
import { DropdownPlugin } from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import i18n from './i18n'

Vue.config.productionTip = false

Vue.use(DropdownPlugin)

new Vue({
  i18n,
  render: h => h(App)
}).$mount('#app')
