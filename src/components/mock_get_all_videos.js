export default [
  {
    src: '/ru_3.mp4',
    question: {
      ru: 'Что Вас спросили?',
      en: 'Hi, do you want a juice?',
      es: 'Hola, ¿Quieres un jugo?',
      fr: 'Salut, tu veux du jus ?',
    },
    answers: {
      ru: [
        {
          text: 'Хотите ли вы пить сок?',
          is_right: true,
        },
        {
          text: 'Пили ли вы когда-нибудь апельсиновый сок',
        },
        {
          text: 'У Вас попросили сок',
        },
      ],
      en: [
        {
          text: 'Have you ever drink an orange juice?',
        },
        {
          text: 'Hi, do you want a juice?',
          is_right: true,
        },
        {
          text: 'Can you give me a juice?',
        },
      ],
      es: [
        {
          text: '¿Has bebido alguna vez un zumo de naranja?',
        },
        {
          text: 'Por favor dame un poco de jugo',
        },
        {
          text: 'Hola, ¿Quieres un jugo?',
          is_right: true,
        },
      ],
      fr: [
        {
          text: 'Pouvez-vous me donner du jus?',
        },
        {
          text: 'Salut, tu veux du jus ?',
          is_right: true,
        },
        {
          text: 'Avez-vous déjà bu un jus d\'orange?',
        },
      ]
    }
  },
  {
    src: '/ru_2.mp4',
    question: {
      ru: 'Что Вас спросили?',
      en: 'Hi, do you want a juice?',
      es: 'Hola, ¿Quieres un jugo?',
      fr: 'Salut, tu veux du jus ?',
    },
    answers: {
      ru: [
        {
          text: 'Хотите ли вы пить сок?',
          is_right: true,
        },
        {
          text: 'Пили ли вы когда-нибудь апельсиновый сок',
        },
        {
          text: 'У Вас попросили сок',
        },
      ],
      en: [
        {
          text: 'Have you ever drink an orange juice?',
        },
        {
          text: 'Hi, do you want a juice?',
          is_right: true,
        },
        {
          text: 'Can you give me a juice?',
        },
      ],
      es: [
        {
          text: '¿Has bebido alguna vez un zumo de naranja?',
        },
        {
          text: 'Por favor dame un poco de jugo',
        },
        {
          text: 'Hola, ¿Quieres un jugo?',
          is_right: true,
        },
      ],
      fr: [
        {
          text: 'Pouvez-vous me donner du jus?',
        },
        {
          text: 'Salut, tu veux du jus ?',
          is_right: true,
        },
        {
          text: 'Avez-vous déjà bu un jus d\'orange?',
        },
      ]
    }
  },
  {
    src: '/ru_1.mp4',
    question: {
      ru: 'Что Вас спросили?',
      en: 'Hi, do you want a juice?',
      es: 'Hola, ¿Quieres un jugo?',
      fr: 'Salut, tu veux du jus ?',
    },
    answers: {
      ru: [
        {
          text: 'Хотите ли вы пить сок?',
          is_right: true,
        },
        {
          text: 'Пили ли вы когда-нибудь апельсиновый сок',
        },
        {
          text: 'У Вас попросили сок',
        },
      ],
      en: [
        {
          text: 'Have you ever drink an orange juice?',
        },
        {
          text: 'Hi, do you want a juice?',
          is_right: true,
        },
        {
          text: 'Can you give me a juice?',
        },
      ],
      es: [
        {
          text: '¿Has bebido alguna vez un zumo de naranja?',
        },
        {
          text: 'Por favor dame un poco de jugo',
        },
        {
          text: 'Hola, ¿Quieres un jugo?',
          is_right: true,
        },
      ],
      fr: [
        {
          text: 'Pouvez-vous me donner du jus?',
        },
        {
          text: 'Salut, tu veux du jus ?',
          is_right: true,
        },
        {
          text: 'Avez-vous déjà bu un jus d\'orange?',
        },
      ]
    }
  }
]
