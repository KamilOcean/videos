export default [
  {
    src: '/fr_1.mp4',
    question: 'Le café',
    answers: [
      {
        text: 'Кофе',
        is_right: true,
      },
      {
        text: 'Чай',
      },
      {
        text: 'Квас',
      },
    ],
  },
  {
    src: '/fr_2.mp4',
    subtitiles: 'bonjour comment ça va',
    question: 'Что сказал автор?',
    answers: [
      {
        text: 'Привет, как оно',
        is_right: true,
      },
      {
        text: 'Доброе утро',
      },
      {
        text: 'До свидания',
      },
    ],
  },
  {
    src: '/fr_3.mp4',
    question: 'Что за блюдо?',
    answers: [
      {
        text: 'Яичница',
      },
      {
        text: 'Овсянка',
      },
      {
        text: 'Картофель пюре',
        is_right: true,
      },
    ],
  },
  {
    src: '/fr_4.mp4',
    question: 'О чем говорит автор?',
    answers: [
      {
        text: 'О смерти',
      },
      {
        text: 'О еде',
      },
      {
        text: 'О музыке',
        is_right: true,
      },
    ],
  },
  {
    src: '/fr_5.mp4',
    question: 'Une banane',
    answers: [
      {
        text: 'Орех',
      },
      {
        text: 'Огурец',
      },
      {
        text: 'Банан',
        is_right: true,
      },
    ],
  },
]
