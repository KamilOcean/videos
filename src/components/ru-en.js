export default [
  {
    src: '/ru_1.mp4',
    question: {
      en: 'What is Ilya Lagutenko singing about?',
    },
    answers: {
      en: [
        {
          text: 'He wants not to be found',
        },
        {
          text: 'He knows where you live',
        },
        {
          text: 'He will break through to you',
          is_right: true,
        },
      ],
    }
  },
  {
    src: '/ru_2.mp4',
    question: {
      en: 'Who is Ilya Rublev?',
    },
    answers: {
      en: [
        {
          text: 'Musician',
        },
        {
          text: 'Sales Representative',
        },
        {
          text: 'Business trainer',
          is_right: true,
        },
      ],
    }
  }
]
