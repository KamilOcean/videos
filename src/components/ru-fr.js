export default [
  {
    src: '/ru_1.mp4',
    question: 'De quoi chante Ilya Lagutenko ?',
    answers: [
      {
        text: 'Il veut ne pas être trouvé',
      },
      {
        text: 'Il percera jusqu\'à toi',
        is_right: true,
      },
      {
        text: 'Il sait où tu habites',
      },
    ]
  },
  {
    src: '/ru_2.mp4',
    question: 'Qui est Ilya Roublev ?',
    answers: [
      {
        text: 'Musicien',
      },
      {
        text: 'Formateur en entreprise',
        is_right: true,
      },
      {
        text: 'Commercial',
      },
    ]
  },
	{
    src: '/ru_3.mp4',
    question: 'Qu\'a fait Faustus ?',
    answers: [
      {
        text: 'J\'ai laissé tomber ma petite amie enceinte',
      },
      {
        text: 'A bu le poison',
      },
      {
				text: 'Vendu mon âme',
				is_right: true,
      },
    ],
  },
	{
    src: '/ru_4.mp4',
    question: 'Que faisait Zemfira ?',
    answers: [
      {
        text: 'je cherchais une issue',
      },
      {
        text: 'Manqué',
      },
      {
				text: 'je te cherchais',
				is_right: true,
      },
    ],
  },
]
