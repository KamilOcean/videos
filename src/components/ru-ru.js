export default [
  {
    src: '/ru_1.mp4',
    question: 'О чём поёт Илья Лагутенко?',
    answers: [
      {
        text: 'Он прорвётся к тебе',
        is_right: true,
      },
      {
        text: 'Он хочет, чтобы его не смогли найти',
      },
      {
        text: 'Он знает где ты живёшь',
      },
    ],
  },
  {
    src: '/ru_2.mp4',
    question: 'Кто такой Илья Рублёв?',
    answers: [
      {
        text: 'Музыкант',
      },
      {
        text: 'Бизнес-тренер',
        is_right: true,
      },
      {
        text: 'Торговый представитель',
      },
    ],
  },
  {
    src: '/ru_3.mp4',
    question: 'Что сделал Фауст?',
    answers: [
      {
        text: 'Бросил свою беременную девушку',
      },
      {
        text: 'Выпил яд',
      },
      {
				text: 'Продал душу',
				is_right: true,
      },
    ],
  },
	{
    src: '/ru_4.mp4',
    question: 'Что делала Земфира?',
    answers: [
      {
        text: 'Искала выход',
      },
      {
        text: 'Скучала',
      },
      {
				text: 'Искала тебя',
				is_right: true,
      },
    ],
  },
]
