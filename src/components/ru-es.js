export default [
  {
    src: '/ru_1.mp4',
    question: '¿De qué está cantando Ilya Lagutenko?',
    answers:[
      {
        text: 'Él se abrirá paso hacia ti',
        is_right: true,
      },
      {
        text: 'El quiere no ser encontrado',
      },
      {
        text: 'Él sabe dónde vives',
      },
    ],
  },
  {
    src: '/ru_2.mp4',
    question: '¿Quién es Ilya Rublev?',
    answers: [
      {
        text: 'Entrenador de negocios',
        is_right: true,
      },
      {
        text: 'Músico',
      },
      {
        text: 'Representante de ventas',
      },
    ],
  },
  {
    src: '/ru_3.mp4',
    question: '¿Qué estaba haciendo Zemfira?',
    answers: [
      {
        text: 'Estaba buscando una salida',
      },
      {
        text: 'Omitido',
      },
      {
				text: 'te estaba buscando',
				is_right: true,
      },
    ],
  },
]
